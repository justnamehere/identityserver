// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System.Collections.Generic;

namespace Test
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new IdentityResource[]
            {
                new IdentityResources.OpenId()
            };
        }

        public static IEnumerable<ApiResource> GetApis()
        {
            return new List<ApiResource>{
                new ApiResource("api1", "My API")
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            var clients = new List<Client>();
            var client = new Client
            {
                ClientId = "client",
                AllowedGrantTypes = GrantTypes.ClientCredentials,

                ClientSecrets = { new Secret("secret".Sha256())
            },
                AllowedScopes = { "api1" }
            };

            var client0 = new Client
            {
                ClientId = "client_p",
                AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                ClientSecrets = { new Secret("secret".Sha256())
            },
                AllowedScopes = { "api1" }
            };

            var client1 = new Client
            {
                ClientId = "mvc",
                ClientName = "MVC Client",
                AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,

                RequireConsent = false,

                ClientSecrets = {new Secret("secret".Sha256())},

                RedirectUris = { "http://localhost:5002/signin-oidc" },
                PostLogoutRedirectUris = { "http://localhost:5002/signout-callback-oidc" },

                AllowedScopes ={
                IdentityServerConstants.StandardScopes.OpenId,
                IdentityServerConstants.StandardScopes.Profile,
                "api1"},

                AllowOfflineAccess = true
            };

            clients.Add(client);
            clients.Add(client0);
            clients.Add(client1);

            return clients;
        }

        public static List<TestUser> GetUsers()
        {
            var users = new List<TestUser>();

            var user0 = new TestUser
            {
                SubjectId = "1",
                Username = "alice",
                Password = "password"
            };
            users.Add(user0);

            return users;
        }
    }
}